package com.intertad.team.service.base.impl;

import com.intertad.team.util.constant.MessageCode;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Service;

import java.util.Locale;

@Slf4j
@RequiredArgsConstructor
@Service
public class MessageServiceImpl implements MessageService {

    private final MessageSource messageSource;

    @Override
    public String getMessage(MessageCode messageCode, Locale locale, Object... args) {
        try {
            return messageSource.getMessage(messageCode.name(), args, messageCode.getDefaultMessage(), locale != null ? locale : getLocale());
        } catch (Exception ex) {
            log.warn("error during preparing message", ex);
            return String.format(messageCode.getDefaultMessage(), args);
        }
    }

    @Override
    public String getMessage(MessageCode messageCode, Object... args) {
        return getMessage(messageCode, getLocale(), args);
    }

    @Override
    public String getMessage(String code, String defaultMessage, Object... args) {
        try {
            return messageSource.getMessage(code, args, defaultMessage, getLocale());
        } catch (Exception ex) {
            log.warn("error during preparing message", ex);
            return String.format(defaultMessage, args);
        }
    }

    private Locale getLocale() {
        return LocaleContextHolder.getLocale();
    }
}
