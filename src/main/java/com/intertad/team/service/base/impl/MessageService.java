package com.intertad.team.service.base.impl;


import com.intertad.team.util.constant.MessageCode;

import java.util.Locale;

public interface MessageService {

    String getMessage(MessageCode messageCode, Locale locale, Object... args);

    String getMessage(MessageCode messageCode, Object... args);

    String getMessage(String code, String defaultMessage, Object... args);
}
