package com.intertad.team.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intertad.team.model.SimpleMessage;
import com.intertad.team.model.WatiSimpleMessageResponse;
import com.intertad.team.service.IWatiAPI;
import com.intertad.team.util.ExceptionUtils;
import com.intertad.team.util.constant.MessageCode;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;

import java.util.Map;

@Service
@Slf4j
@RequiredArgsConstructor
public class WatiAPIImpl implements IWatiAPI {

    private final ObjectMapper objectMapper;
    private final ExceptionUtils exceptionUtils;

    @Value("${project.wati.base-url}")
    private String WATI_BASE_URL;
    @Value("${project.wati.send-simple-message-url}")
    private String WATI_SEND_SIMPLE_MESSAGE_URL;

    private final static String AUTH_TOKEN = "Authorization";
    private final static String MESSAGE_TEXT_LABEL = "messageText";
    private final static String WATI_RESULT_LABEL = "result";
    private final static String WATI_INFO_LABEL = "message";
    private final static String WATI_SUCCESS_LABEL = "success";
// API - remove
    // Wati
    // Api
    @Override
    @SneakyThrows
    public WatiSimpleMessageResponse sendSimpleMessage(SimpleMessage simpleMessageDTO) {
        WebClient webClient = WebClient.create(WATI_BASE_URL);
        Mono<String> response = webClient.post()
                .uri(WATI_SEND_SIMPLE_MESSAGE_URL + simpleMessageDTO.getRecipient())
                .body(BodyInserters.fromFormData(MESSAGE_TEXT_LABEL, simpleMessageDTO.getText()))
                .header(AUTH_TOKEN, String.valueOf("Bearer " + simpleMessageDTO.getWatiToken()))
                .retrieve()
                .bodyToMono(String.class);
        Map<String, Object> responseMap = objectMapper.readValue(response.block(), Map.class);
        return formatResponse(responseMap);
    }

    public WatiSimpleMessageResponse formatResponse(Map<String, Object> responseMap) {
        if (responseMap != null && responseMap.containsKey(WATI_RESULT_LABEL)) {
            if (isResponseSuccess(responseMap)) {
                return new WatiSimpleMessageResponse(Boolean.TRUE, WATI_SUCCESS_LABEL);
            } else {
                return buildUnsuccessfulResponse(String.valueOf(responseMap.get(WATI_INFO_LABEL)));
            }
        } else {
            return buildUnsuccessfulResponse(MessageCode.ERROR_WHILE_SENDING_SIMPLE_MESSAGE_TO_WATI.getDefaultMessage());
        }
    }

    public boolean isResponseSuccess(Map<String, Object> response) {
        return response.get(WATI_RESULT_LABEL) instanceof String;
    }

    public WatiSimpleMessageResponse buildUnsuccessfulResponse(String message) {
        return new WatiSimpleMessageResponse(Boolean.FALSE, message);
    }

}
