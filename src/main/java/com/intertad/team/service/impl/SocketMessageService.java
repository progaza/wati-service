package com.intertad.team.service.impl;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intertad.team.model.SimpleMessageReceiver;
import com.intertad.team.model.WatiSimpleMessageReceiver;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.messaging.simp.SimpMessageSendingOperations;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class SocketMessageService {

    private final SimpMessageSendingOperations messagingTemplate;
    private final ObjectMapper objectMapper;

    @SneakyThrows
    public void sendMessage(String listenersUrl, String message) {
        messagingTemplate.convertAndSend(
                listenersUrl,
                new SimpleMessageReceiver(objectMapper.readValue(message, WatiSimpleMessageReceiver.class)));
    }
}
