package com.intertad.team.service;

import com.intertad.team.model.SimpleMessage;
import com.intertad.team.model.WatiSimpleMessageResponse;

public interface IWatiAPI {
    WatiSimpleMessageResponse sendSimpleMessage(SimpleMessage simpleMessageDTO);
}
