package com.intertad.team.exception;

import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.ToString;

@Data
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = true)
public class ReserveOrderException extends RuntimeException {

    public ReserveOrderException() {
    }

    public ReserveOrderException(String message) {
        super(message);
    }

    public ReserveOrderException(String message, Throwable cause) {
        super(message, cause);
    }

    public ReserveOrderException(Throwable cause) {
        super(cause);
    }

    public ReserveOrderException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
