package com.intertad.team.exception;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.intertad.team.util.DateUtils;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.validation.ObjectError;

import java.time.LocalDateTime;
import java.util.List;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ApiError {
    @JsonFormat(pattern = DateUtils.ISO_DATE_TIME_FORMAT_STRING)
    private final LocalDateTime timestamp = LocalDateTime.now();
    private int status = 500;
    private String error;
    private String errorId;
    private String message;
    private String errorCode;
    private List<ObjectError> details;
    private String path = "";
    private String stacktrace;
}
