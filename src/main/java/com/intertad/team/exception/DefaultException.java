package com.intertad.team.exception;

import lombok.Getter;
import lombok.Setter;
import org.springframework.http.HttpStatus;

@Getter
@Setter
public class DefaultException extends RuntimeException {

    private final String message;
    private Object[] args;
    private final int status;
    private int errorCode;

    public DefaultException(String message, Throwable cause) {
        super(message, cause);
        this.message = message;
        this.status = HttpStatus.BAD_REQUEST.value();
    }

    public DefaultException(String message, Object... args) {
        super(message);
        this.message = message;
        this.status = HttpStatus.BAD_REQUEST.value();
        this.args = args;
    }

    public DefaultException(String message, int errorCode) {
        super(message);
        this.message = message;
        this.status = HttpStatus.BAD_REQUEST.value();
        this.errorCode = errorCode;
    }

}
