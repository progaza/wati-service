package com.intertad.team.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
public class SimpleMessageReceiver {
    private String id;
    private String createDate;
    private String sender;
    private String text;

    public SimpleMessageReceiver(WatiSimpleMessageReceiver receiver) {
        this.id = receiver.getId();
        this.createDate = receiver.getCreated();
        this.sender = receiver.getWaId();
        this.text = receiver.getText();
    }
}
