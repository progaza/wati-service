package com.intertad.team.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class WatiSimpleMessageReceiver {
    private String id;
    private String created;
    private String text;
    private String waId;
}
