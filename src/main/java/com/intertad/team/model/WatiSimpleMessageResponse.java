package com.intertad.team.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class WatiSimpleMessageResponse {
    private boolean result;//false,
    private String info;//"Ticket has not valid"
}
