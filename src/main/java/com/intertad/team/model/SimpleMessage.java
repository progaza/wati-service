package com.intertad.team.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class SimpleMessage {
    @NotNull
    private String text;
    @NotNull
    private String recipient;
    @NotNull
    private String watiToken;// table
}
