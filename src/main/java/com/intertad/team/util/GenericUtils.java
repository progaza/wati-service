package com.intertad.team.util;



import com.intertad.team.exception.DefaultException;

import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;


public class GenericUtils {

    private GenericUtils() {

    }

    /**
     * Define generic class
     *
     * @param clazz    the clazz
     * @param argument the argument
     * @return the class
     */
    public static Class defineGenericClass(Class clazz, int argument) {
        try {
            Type type = ((ParameterizedType) clazz.getGenericSuperclass()).getActualTypeArguments()[argument];
            if (type instanceof Class) {
                return (Class) type;
            } else {
                return Class.forName(type.getTypeName());
            }
        } catch (Exception ex) {
            throw new DefaultException("The type could not be defined");
        }
    }

}
