package com.intertad.team.util;

import com.intertad.team.exception.DefaultException;
import com.intertad.team.service.base.impl.MessageService;
import com.intertad.team.util.constant.MessageCode;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ExceptionUtils {

    private final MessageService messageService;

    public void throwDefaultException(MessageCode messageCode, Object... args) {
        String message = messageService.getMessage(messageCode, args);
        throw new DefaultException(message, messageCode.getErrorCode());
    }

    public DefaultException getDefaultException(MessageCode messageCode, Object... args) {
        String message = messageService.getMessage(messageCode, args);
        return new DefaultException(message, messageCode.getErrorCode());
    }
}
