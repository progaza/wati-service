package com.intertad.team.util.constant;

public enum Status {
    DRAFT,
    PENDING,
    PRE_ACTIVE,
    ACTIVE,
    DELETED,
    ARCHIVED,
    CANCELLED,
    SUCCESS,
    NONE,

    PENDING_APPROVAL,
    APPROVED,
    CONFIRMED,
    REJECTED,

    CREATED,
    ERROR
}
