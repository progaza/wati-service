package com.intertad.team.util.constant;

public enum MessageCode {

    ERROR_WHILE_SENDING_SIMPLE_MESSAGE_TO_WATI(1001, "Error was occur while sending simple message to Wati services");

    int errorCode;
    private String defaultMessage;

    MessageCode(int code, String defaultMessage) {
        this.errorCode = code;
        this.defaultMessage = defaultMessage;
    }

    public String getDefaultMessage() {
        return defaultMessage;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
