package com.intertad.team;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WatiJavaAPIApplication {

    public static void main(String[] args) {
        SpringApplication.run(WatiJavaAPIApplication.class, args);
    }

}

