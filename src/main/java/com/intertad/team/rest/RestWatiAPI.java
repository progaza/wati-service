package com.intertad.team.rest;

import com.intertad.team.model.Greeting;
import com.intertad.team.model.SimpleMessage;
import com.intertad.team.model.WatiSimpleMessageResponse;
import com.intertad.team.service.IWatiAPI;
import com.intertad.team.service.impl.SocketMessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;


@RestController
@Slf4j
@RequiredArgsConstructor
public class RestWatiAPI {

    private final IWatiAPI watiAPI;
    private final SocketMessageService socketMessageService;

    private static final String PATH = "/wati";
    private static final String PATH_SIMPLE_MESSAGE = PATH + "/simple-message";
    private static final String PATH_SIMPLE_MESSAGE_FROM_WATI_RECEIVER = "/app/simple-message";

    @PostMapping(value = PATH_SIMPLE_MESSAGE)
    public WatiSimpleMessageResponse createPost(@RequestBody @Valid @NotNull SimpleMessage simpleMessageDTO) {
        return watiAPI.sendSimpleMessage(simpleMessageDTO);
    }

    @PostMapping(value = PATH_SIMPLE_MESSAGE_FROM_WATI_RECEIVER)
    public String createPost(@RequestBody String object) {
        log.info("Received message from WATI: " + object);
        socketMessageService.sendMessage("/topic/simple-messages", object);
        return "success";
    }
}
