package com.intertad.team.rest;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.intertad.team.model.Greeting;
import com.intertad.team.model.HelloMessage;
import com.intertad.team.model.WatiSimpleMessageReceiver;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.util.HtmlUtils;

@RestController
@RequiredArgsConstructor
@Slf4j
public class GreetingController {

    private final ObjectMapper objectMapper;

    @MessageMapping("/hello")
    @SendTo("/topic/simple-messages")
    public Greeting greeting(HelloMessage message) throws Exception {
        Thread.sleep(1000); // simulated delay
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()) + "!");
    }

}
