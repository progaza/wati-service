Service only send a simple message and receive a simple message from client.
As a 'simple' message we say only 'text' type messages.  

**First of all**, client should start a conversation scanning by QR code or chat to the operator with certain code in WhatsApp.

1. ##### To send message to a client we should use following service:

~~~~
    url: http://195.133.146.103:8585/wati/simple-message
    method: POST
    body(example): { "recipient":"77474089174", "text":"Your order status is updated!", "watiToken":"eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJqdGkiOiI2MjdiZmY4NS04NzU5LTRkOWUtODk1ZS03ZjQ1NWQyNTYzYWEiLCJ1bmlxdWVfbmFtZSI6ImVhZ2FtZXIuMDYwQGdtYWlsLmNvbSIsIm5hbWVpZCI6ImVhZ2FtZXIuMDYwQGdtYWlsLmNvbSIsImVtYWlsIjoiZWFnYW1lci4wNjBAZ21haWwuY29tIiwiaHR0cDovL3NjaGVtYXMubWljcm9zb2Z0LmNvbS93cy8yMDA4LzA2L2lkZW50aXR5L2NsYWltcy9yb2xlIjoiVFJJQUwiLCJleHAiOjE2MTY2MzA0MDAsImlzcyI6IkNsYXJlX0FJIiwiYXVkIjoiQ2xhcmVfQUkifQ.Q2tujR3S264S-UYwkiAz_8FG7mcCa3jtWeg5ztK9rAk" }
    response: { "result": true, "info": "success" }
~~~~

Body properties:  
**receiver** - clients phone number(without sign '+', with country code)  
**text** - a text message  
**watiToken** - a unique token that can be got from https://app.wati.io/api-docs. Only token, without key 'Bearer'.
  
Result properties:  
- if occur an error somehow, 'result' key has the value 'false' and 'info' key has a cause message.  
Example of error response:  
~~~~
    { "result": false, "info": "Ticket has expired" }
~~~~

2. #### To receive using webhook front should connect following service(sockjs):  
~~~~
    url to connect sockjs: ws://195.133.146.103:8585/wati-websocket
    url to listen messages: '/topic/simple-messages'
    respone(example): {"id":"60575203f566630551f48152","createDate":"2021-03-21T14:02:43.3459678Z","sender":"77474089174","text":"Know how WATI works?"}
~~~~
Response properties:  
**id** - a unique identifier from wati
**createDate** - when message is created
**sender** - client's phone number
**text** - text message from client

#### As an example to connecting to the websocket following JS script can help you:
```
function connect() {
    var socket = new SockJS('http://195.133.146.103:8585/wati-websocket');
    stompClient = Stomp.over(socket);
    stompClient.connect({}, function (frame) {
        setConnected(true);
        stompClient.subscribe('/topic/simple-messages', function (greeting) {
            showGreeting(JSON.parse(greeting.body));
        });
    });
}
```
